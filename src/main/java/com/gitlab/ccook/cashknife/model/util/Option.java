package com.gitlab.ccook.cashknife.model.util;

import java.util.Objects;

public class Option<T> {
    private T thing;

    public Option(T thing) {
        this.thing = thing;
    }

    public Option() {
    }

    public T get() {
        return thing;
    }

    public boolean isDefined() {
        return null != thing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Option<?> option = (Option<?>) o;
        return Objects.equals(thing, option.thing);
    }

    @Override
    public int hashCode() {
        return Objects.hash(thing);
    }

    @Override
    public String toString() {
        if (thing == null) {
            return "None";
        } else {
            return thing.toString();
        }
    }
}
