package com.gitlab.ccook.cashknife.model.tables;

import com.gitlab.ccook.cashknife.model.elements.Book;

public class Books extends GNUCashTable<Book> {

    public Books() {
        super(Book.class);
    }
}
