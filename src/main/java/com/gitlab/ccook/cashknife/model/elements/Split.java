package com.gitlab.ccook.cashknife.model.elements;

import com.gitlab.ccook.cashknife.model.tables.Accounts;
import com.gitlab.ccook.cashknife.model.tables.Transactions;
import com.gitlab.ccook.cashknife.model.util.CashKnifeException;
import com.gitlab.ccook.cashknife.model.util.Option;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

/*
CREATE TABLE splits(guid text(32) PRIMARY KEY NOT NULL,
                    tx_guid text(32) NOT NULL,
                    account_guid text(32) NOT NULL,
                     memo text(2048) NOT NULL,
                      action text(2048) NOT NULL,
                       reconcile_state text(1) NOT NULL,
                       reconcile_date text(19),
                       value_num bigint NOT NULL,
                       value_denom bigint NOT NULL,
                        quantity_num bigint NOT NULL,
                        quantity_denom bigint NOT NULL,
                        lot_guid text(32));

 */
public class Split extends GNUCashElement {
    public static final String TX_GUID = "tx_guid";
    public static final String ACCOUNT_GUID = "account_guid";
    public static final String MEMO = "memo";
    public static final String ACTION = "action";
    public static final String RECONCILE_STATE = "reconcile_state";
    public static final String RECONCILE_DATE = "reconcile_date";
    public static final String VALUE_NUM = "value_num";
    public static final String VALUE_DENOM = "value_denom";
    public static final String QUANTITY_NUM = "quantity_num";
    public static final String QUANTITY_DENOM = "quantity_denom";
    public static final String LOT_GUID = "lot_guid";
    private String guid;
    private Account account;
    private Transaction transaction;
    private String memo;
    private String action;
    private String reconcileState;
    private String reconcileDate;
    private BigInteger valueNum;
    private BigInteger valueDenom;
    private BigInteger quantityNum;
    private BigInteger quantityDenom;
    private Option<String> lotId = new Option<>();

    public Split(String guid,
                 Account account,
                 Transaction transaction,
                 String memo,
                 String action,
                 String reconcileState,
                 String reconcileDate,
                 BigInteger valueNum,
                 BigInteger valueDenom,
                 BigInteger quantityNum,
                 BigInteger quantityDenom,
                 Option<String> lotGUID) {

        this.guid = guid;
        this.account = account;
        this.transaction = transaction;
        this.memo = memo;
        this.action = action;
        this.reconcileState = reconcileState;
        this.reconcileDate = reconcileDate;
        this.valueNum = valueNum;
        this.valueDenom = valueDenom;
        this.quantityNum = quantityNum;
        this.quantityDenom = quantityDenom;
        this.lotId = lotGUID;
    }

    public Split(Connection conn, ResultSet s) throws SQLException, CashKnifeException {
        this(s.getString(Split.GUID),
                Accounts.getElement(conn, s.getString(Split.ACCOUNT_GUID)),
                Transactions.getElement(conn, s.getString(Split.TX_GUID)),
                s.getString(Split.MEMO),
                s.getString(Split.ACTION),
                s.getString(Split.RECONCILE_STATE),
                s.getString(Split.RECONCILE_DATE),
                BigInteger.valueOf(s.getBigDecimal(Split.VALUE_NUM).longValue()),
                BigInteger.valueOf(s.getBigDecimal(Split.VALUE_DENOM).longValue()),
                BigInteger.valueOf(s.getBigDecimal(Split.QUANTITY_NUM).longValue()),
                BigInteger.valueOf(s.getBigDecimal(Split.QUANTITY_DENOM).longValue()),
                new Option<>(s.getString(Split.LOT_GUID))
        );
    }


    public String getGuid() {
        return guid;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public String getMemo() {
        return memo;
    }

    public String getAction() {
        return action;
    }

    public String getReconcileState() {
        return reconcileState;
    }

    public String getReconcileDate() {
        return reconcileDate;
    }

    public BigInteger getValueNum() {
        return valueNum;
    }

    public BigInteger getValueDenom() {
        return valueDenom;
    }

    public BigInteger getQuantityNum() {
        return quantityNum;
    }

    public BigInteger getQuantityDenom() {
        return quantityDenom;
    }

    public Option<String> getLotId() {
        return lotId;
    }

    public Account getAccount() {
        return account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Split split = (Split) o;
        return Objects.equals(guid, split.guid) &&
                Objects.equals(account, split.account) &&
                Objects.equals(transaction, split.transaction) &&
                Objects.equals(memo, split.memo) &&
                Objects.equals(action, split.action) &&
                Objects.equals(reconcileState, split.reconcileState) &&
                Objects.equals(reconcileDate, split.reconcileDate) &&
                Objects.equals(valueNum, split.valueNum) &&
                Objects.equals(valueDenom, split.valueDenom) &&
                Objects.equals(quantityNum, split.quantityNum) &&
                Objects.equals(quantityDenom, split.quantityDenom) &&
                Objects.equals(lotId, split.lotId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(guid, account, transaction, memo, action, reconcileState, reconcileDate, valueNum, valueDenom, quantityNum, quantityDenom, lotId);
    }

    @Override
    public String toString() {
        return "Split{" +
                "guid='" + guid + '\'' +
                ", account=" + account +
                ", transaction=" + transaction +
                ", memo='" + memo + '\'' +
                ", action='" + action + '\'' +
                ", reconcileState='" + reconcileState + '\'' +
                ", reconcileDate='" + reconcileDate + '\'' +
                ", valueNum=" + valueNum +
                ", valueDenom=" + valueDenom +
                ", quantityNum=" + quantityNum +
                ", quantityDenom=" + quantityDenom +
                ", lotId=" + lotId +
                '}';
    }
}
