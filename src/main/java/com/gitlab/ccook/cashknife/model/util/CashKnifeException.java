package com.gitlab.ccook.cashknife.model.util;

import java.sql.SQLException;

public class CashKnifeException extends Exception {
    public CashKnifeException(Throwable e) {
        super(e);
    }
}
