package com.gitlab.ccook.cashknife.model.tables;

import com.gitlab.ccook.cashknife.model.elements.Version;

public class Versions extends GNUCashTable<Version> {

    public Versions() {
        super(Version.class);
    }

}
