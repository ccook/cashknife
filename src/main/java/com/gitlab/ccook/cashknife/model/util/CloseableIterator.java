package com.gitlab.ccook.cashknife.model.util;

import com.gitlab.ccook.cashknife.model.elements.GNUCashElement;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Objects;

public abstract class CloseableIterator<T extends GNUCashElement> implements Iterator<T>, Closeable {
    private ResultSet s;
    protected Connection conn;

    public CloseableIterator(Connection conn, ResultSet s) {
        this.s = s;
        this.conn = conn;
    }

    @Override
    public void close() throws IOException {
        try {
            s.close();
        } catch (SQLException e) {
            throw new IOException(e);
        }
    }

    @Override
    public boolean hasNext() {
        try {
            return s.next();
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public T next() {
        try {
            return nextElement(s);
        } catch (CashKnifeException e) {
            return null;
        }
    }

    protected abstract T nextElement(ResultSet s) throws CashKnifeException;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CloseableIterator<?> that = (CloseableIterator<?>) o;
        return Objects.equals(s, that.s) &&
                Objects.equals(conn, that.conn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(s, conn);
    }

    @Override
    public String toString() {
        return "CloseableIterator{" +
                "s=" + s +
                ", conn=" + conn +
                '}';
    }
}
