package com.gitlab.ccook.cashknife.model.tables;

import com.gitlab.ccook.cashknife.model.util.CashKnifeException;
import com.gitlab.ccook.cashknife.model.elements.Commodity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Commodities extends GNUCashTable<Commodity> {

    public Commodities() {
        super(Commodity.class);
    }


    public static Commodity getElement(Connection conn, String string) throws CashKnifeException {
        try {
            PreparedStatement preparedStatement = conn.prepareStatement("select * from commodities where guid =\"" + string + "\"");
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return new Commodity(conn, resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw new CashKnifeException(e);
        }
    }


}
