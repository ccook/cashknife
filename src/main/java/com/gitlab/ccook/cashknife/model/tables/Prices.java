package com.gitlab.ccook.cashknife.model.tables;

import com.gitlab.ccook.cashknife.model.elements.Price;


public class Prices extends GNUCashTable<Price> {

    public Prices() {
        super(Price.class);
    }
}
