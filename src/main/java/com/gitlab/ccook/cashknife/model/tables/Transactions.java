package com.gitlab.ccook.cashknife.model.tables;

import com.gitlab.ccook.cashknife.model.elements.Commodity;
import com.gitlab.ccook.cashknife.model.elements.Transaction;
import com.gitlab.ccook.cashknife.model.util.CashKnifeException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transactions extends GNUCashTable<Transaction> {
    public Transactions() {
        super(Transaction.class);
    }

    public static Transaction getElement(Connection conn, String string) throws CashKnifeException {
        try {
            PreparedStatement preparedStatement = conn.prepareStatement("select * from transactions where guid =\"" + string + "\"");
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return new Transaction(conn, resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw new CashKnifeException(e);
        }
    }
}
