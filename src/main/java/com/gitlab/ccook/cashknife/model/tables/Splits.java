package com.gitlab.ccook.cashknife.model.tables;

import com.gitlab.ccook.cashknife.model.elements.Split;

public class Splits extends GNUCashTable<Split> {
    public Splits() {
        super(Split.class);
    }
}
