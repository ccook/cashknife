package com.gitlab.ccook.cashknife.model.elements;

import com.gitlab.ccook.cashknife.model.tables.Commodities;
import com.gitlab.ccook.cashknife.model.util.CashKnifeException;
import com.gitlab.ccook.cashknife.model.util.Option;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Price extends GNUCashElement {
    public static String COMMODITY_GUID = "commodity_guid";
    public static String CURRENCY_GUID = "currency_guid";
    public static String DATE = "date";
    public static String SOURCE = "source";
    public static String TYPE = "type";
    public static String VALUE_NUM = "value_num";
    public static String VALUE_DENOM = "value_denom";

    private String guid;
    private Commodity commodity;
    private Commodity currency;
    //'2018-10-05 04:00:00
    private String date;
    private Option<String> source = new Option<>();
    private Option<String> type = new Option<>();
    private BigInteger valueNum;
    private BigInteger valueDenom;


    public Price(String guid,
                 Commodity commodity,
                 Commodity currency,
                 String date,
                 String source,
                 String type,
                 BigInteger valueNum,
                 BigInteger valueDenom) {
        this.guid = guid;
        this.commodity = commodity;
        this.currency = currency;
        this.date = date;
        this.source = new Option<>(source);
        this.type = new Option<>(type);
        this.valueNum = valueNum;
        this.valueDenom = valueDenom;
    }

    public Price(Connection conn, ResultSet s) throws CashKnifeException, SQLException {
        this(s.getString(Price.GUID),
                Commodities.getElement(conn, s.getString(Price.COMMODITY_GUID)),
                Commodities.getElement(conn, s.getString(Price.CURRENCY_GUID)),
                s.getString(Price.DATE),
                s.getString(Price.SOURCE),
                s.getString(Price.TYPE),
                BigInteger.valueOf(s.getBigDecimal(Price.VALUE_NUM).longValue()),
                BigInteger.valueOf(s.getBigDecimal(Price.VALUE_DENOM).longValue())
        );

    }


    @Override
    public String toString() {
        return "Price{" +
                "guid='" + guid + '\'' +
                ", commodity=" + commodity +
                ", currency=" + currency +
                ", date='" + date + '\'' +
                ", source=" + source +
                ", type=" + type +
                ", valueNum=" + valueNum +
                ", valueDenom=" + valueDenom +
                '}';
    }
}
