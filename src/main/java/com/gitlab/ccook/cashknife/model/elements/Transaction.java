package com.gitlab.ccook.cashknife.model.elements;

import com.gitlab.ccook.cashknife.model.tables.Accounts;
import com.gitlab.ccook.cashknife.model.tables.Commodities;
import com.gitlab.ccook.cashknife.model.util.CashKnifeException;
import com.gitlab.ccook.cashknife.model.util.Option;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

/*
CREATE TABLE transactions(guid text(32) PRIMARY KEY NOT NULL,
                          currency_guid text(32) NOT NULL,
                          num text(2048) NOT NULL,
                          post_date text(19),
                           enter_date text(19),
                           description text(2048));
CREATE INDEX tx_post_date_index ON transactions(post_date);

 */
public class Transaction extends GNUCashElement {
    public static final String CURRENCY_GUID = "currency_guid";
    public static final String NUM = "num";
    public static final String POST_DATE = "post_date";
    public static final String ENTER_DATE = "enter_date";
    public static final String DESCRIPTION = "description";

    private String guid;
    private Commodity currency;
    private String num;
    private Option<String> postDate = new Option<>();
    private Option<String> enterDate = new Option<>();
    private Option<String> description = new Option<>();

    public Transaction(String guid,
                       Commodity currency,
                       String num,
                       Option<String> postDate,
                       Option<String> enterDate,
                       Option<String> description) {
        this.guid = guid;
        this.currency = currency;
        this.num = num;
        this.postDate = postDate;
        this.enterDate = enterDate;
        this.description = description;
    }

    public Transaction(Connection conn, ResultSet s) throws SQLException, CashKnifeException {
        this(s.getString(Transaction.GUID),
                Commodities.getElement(conn, s.getString(Transaction.CURRENCY_GUID)),
                s.getString(Transaction.NUM),
                new Option<>(s.getString(Transaction.POST_DATE)),
                new Option<>(s.getString(Transaction.ENTER_DATE)),
                new Option<>(s.getString(Transaction.DESCRIPTION)));
    }

    public String getGuid() {
        return guid;
    }

    public Commodity getCurrency() {
        return currency;
    }

    public String getNum() {
        return num;
    }

    public Option<String> getPostDate() {
        return postDate;
    }

    public Option<String> getEnterDate() {
        return enterDate;
    }

    public Option<String> getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(guid, that.guid) &&
                Objects.equals(currency, that.currency) &&
                Objects.equals(num, that.num) &&
                Objects.equals(postDate, that.postDate) &&
                Objects.equals(enterDate, that.enterDate) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(guid, currency, num, postDate, enterDate, description);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "guid='" + guid + '\'' +
                ", currency=" + currency +
                ", num='" + num + '\'' +
                ", postDate=" + postDate +
                ", enterDate=" + enterDate +
                ", description=" + description +
                '}';
    }
}
