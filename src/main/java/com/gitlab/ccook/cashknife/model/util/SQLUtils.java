
package com.gitlab.ccook.cashknife.model.util;

import com.gitlab.ccook.cashknife.model.util.CashKnifeException;

import java.io.File;
import java.sql.*;

public class SQLUtils {

    public static Connection connect(File f) throws CashKnifeException {
        try {
            Connection connection = null;
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:" + f.getAbsolutePath());
            return connection;
        } catch (SQLException e) {
            throw new CashKnifeException(e);
        }
    }
}

