package com.gitlab.ccook.cashknife.model.elements;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

//CREATE TABLE versions(table_name text(50) PRIMARY KEY NOT NULL, table_version integer NOT NULL);

public class Version extends GNUCashElement {
    public static String TABLE_NAME = "table_name";
    public static String TABLE_VERSION = "table_version";

    private String tableName;
    private int tableVersion;

    public Version(String tableName, int tableVersion) {
        this.tableName = tableName;
        this.tableVersion = tableVersion;
    }

    public Version(Connection conn, ResultSet s) throws SQLException {
        this(s.getString(Version.TABLE_NAME), s.getInt(Version.TABLE_VERSION));
    }

    public String getTableName() {
        return tableName;
    }

    public int getTableVersion() {
        return tableVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Version version = (Version) o;
        return tableVersion == version.tableVersion &&
                Objects.equals(tableName, version.tableName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tableName, tableVersion);
    }

    @Override
    public String toString() {
        return "Version{" +
                "tableName='" + tableName + '\'' +
                ", tableVersion=" + tableVersion +
                '}';
    }
}
