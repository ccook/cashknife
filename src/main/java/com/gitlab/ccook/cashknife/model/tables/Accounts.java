package com.gitlab.ccook.cashknife.model.tables;

import com.gitlab.ccook.cashknife.model.elements.Account;
import com.gitlab.ccook.cashknife.model.elements.Commodity;
import com.gitlab.ccook.cashknife.model.util.CashKnifeException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Accounts extends GNUCashTable<Account> {
    public Accounts() {
        super(Account.class);
    }

    public static Account getElement(Connection conn, String string) throws CashKnifeException {
        try {
            PreparedStatement preparedStatement = conn.prepareStatement("select * from accounts where guid =\"" + string + "\"");
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                return new Account(conn, resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw new CashKnifeException(e);
        }
    }
}
