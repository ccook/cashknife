/*
Copyright 2019 Cam Cook

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.gitlab.ccook.cashknife.model.elements;

import com.gitlab.ccook.cashknife.model.tables.Accounts;
import com.gitlab.ccook.cashknife.model.tables.Commodities;
import com.gitlab.ccook.cashknife.model.util.CashKnifeException;
import com.gitlab.ccook.cashknife.model.util.Option;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

//CREATE TABLE accounts(guid text(32) PRIMARY KEY NOT NULL,
//                      name text(2048) NOT NULL,
//                      account_type text(2048) NOT NULL,
//                      commodity_guid text(32),
//                      commodity_scu integer NOT NULL,
//                      non_std_scu integer NOT NULL,
//                      parent_guid text(32),
//                      code text(2048),
//                      description text(2048),
//                      hidden integer,
//                      placeholder integer);

/**
 * Element: Account
 * Table: accounts
 * Schema: CREATE TABLE accounts(guid text(32) PRIMARY KEY NOT NULL,
 * name text(2048) NOT NULL,
 * account_type text(2048) NOT NULL,
 * commodity_guid text(32),
 * commodity_scu integer NOT NULL,
 * non_std_scu integer NOT NULL,
 * parent_guid text(32),
 * code text(2048),
 * description text(2048),
 * hidden integer,
 * placeholder integer);
 */
public class Account extends GNUCashElement {
    /**
     * Labels used by SQL fields
     */
    enum Label {
        GUID,
        NAME,
        ACCOUNT_TYPE,
        COMMODITY_GUID,
        COMMODITY_SCU,
        NON_STD_SCU,
        PARENT_GUID,
        CODE,
        DESCRIPTION,
        HIDDEN,
        PLACEHOLDER
    }


    /**
     * Account ID (GUID / Primary key)
     */
    private String guid;
    /**
     * Plain Text, human-readable name for the Account (e.g Opening Balances)
     */
    private String name;
    /**
     * Account Type (e.g ROOT, ASSET, BANK, STOCK, MUTUAL, CREDIT, INCOME, EXPENSE, LIABILITY, EQUITY)
     */
    private String accountType;
    /**
     * The underlying Commodity the Account transacts in (e.g USD)
     */
    private Option<Commodity> commodity = new Option<>();
    /**
     * The smallest currency/commodity unit is similar to the fraction of a commodity.
     * It is the smallest amount of the commodity that is tracked in the account.
     * If it is different than the fraction of the commodity to which the account is linked,
     * the field non_std_scu is set to 1 (otherwise the latter is set to 0).
     */
    private String commoditySCU;
    /**
     * the parent account to which the account is attached.
     * All accounts but the root_account should have a parent account.
     */
    private Option<Account> parent = new Option<>();
    /**
     * If commoditySCU is different than the fraction of the commodity to which the account is linked,
     * the field non_std_scu is set to 1 (otherwise the latter is set to 0).
     */
    private Boolean isNonStandardSCU;
    /**
     * Plain-text, human-readable, account code provided by the maintainer
     */
    private Option<String> code = new Option<>();
    /**
     * Plain-text, human-readable, description provided by the maintainer
     */
    private Option<String> description = new Option<>();
    /**
     * if True/1, the account will not be displayed in the GnuCash GUI Accounts tab
     * and can be easily excluded from GnuCash GUI Reports.
     * if False/0, the account will be displayed in the GnuCash GUI Accounts tab.
     */
    private Option<Boolean> hidden = new Option<>();
    /**
     * if True/1, the account cannot be involved in transactions through splits
     * (ie it can only be the parent of other accounts).
     * if False/0, the account can have Splits referring to it (as well as be the parent of other accounts).
     * This field, if True, is also stored as a Slot under the key “placeholder” as a string “true”.
     */
    private Option<Boolean> placeholder = new Option<>();

    public Account(String guid,
                   String name,
                   String accountType,
                   Option<Commodity> commodity,
                   String commoditySCU,
                   Option<Account> parent,
                   String nonStdSCU,
                   Option<String> code,
                   Option<String> description,
                   Option<Boolean> hidden,
                   Option<Boolean> placeholder) {
        this.guid = guid;
        this.name = name;
        this.accountType = accountType;
        this.commodity = commodity;
        this.commoditySCU = commoditySCU;
        this.parent = parent;
        this.isNonStandardSCU = "1".equals(nonStdSCU);
        this.code = code;
        this.description = description;
        this.hidden = hidden;
        this.placeholder = placeholder;
    }


    // used by Accounts.getElement()  Accounts.getElementIterator
    public Account(Connection conn, ResultSet s) throws SQLException, CashKnifeException {
        this(s.getString(Label.GUID.name().toLowerCase()),
                s.getString(Label.NAME.name().toLowerCase()),
                s.getString(Label.ACCOUNT_TYPE.name().toLowerCase()),
                new Option<>(Commodities.getElement(conn, s.getString(Label.COMMODITY_GUID.name().toLowerCase()))),
                s.getString(Label.COMMODITY_SCU.name().toLowerCase()),
                new Option<>(Accounts.getElement(conn, s.getString(Label.PARENT_GUID.name().toLowerCase()))),
                s.getString(Label.NON_STD_SCU.name().toLowerCase()),
                new Option<>(s.getString(Label.CODE.name().toLowerCase())),
                new Option<>(s.getString(Label.DESCRIPTION.name().toLowerCase())),
                new Option<>(s.getInt(Label.HIDDEN.name().toLowerCase()) == 1),
                new Option<>(s.getInt(Label.PLACEHOLDER.name().toLowerCase()) == 1));
    }


    /**
     * @return Account ID (GUID / Primary key)
     */
    public String getID() {
        return guid;
    }

    /**
     * @return Plain Text, human-readable name for the Account (e.g Opening Balances)
     */
    public String getName() {
        return name;
    }

    /**
     * @return Account Type (e.g ROOT, ASSET, BANK, STOCK, MUTUAL, CREDIT, INCOME, EXPENSE, LIABILITY, EQUITY)
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * @return The underlying Commodity the Account transacts in (e.g USD)
     */
    public Option<Commodity> getCommodity() {
        return commodity;
    }

    /**
     * @return The smallest currency/commodity unit is similar to the fraction of a commodity.
     * It is the smallest amount of the commodity that is tracked in the account.
     * If it is different than the fraction of the commodity to which the account is linked,
     * the field non_std_scu is set to 1 (otherwise the latter is set to 0).
     */
    public String getCommoditySCU() {
        return commoditySCU;
    }

    /**
     * @return the parent account to which the account is attached.
     * All accounts but the root_account should have a parent account.
     */
    public Option<Account> getParent() {
        return parent;
    }

    /**
     * @return If commoditySCU is different than the fraction of the commodity to which the account is linked,
     * the field non_std_scu is set to 1 (otherwise the latter is set to 0).
     */
    public boolean isNonStandardSCU() {
        return isNonStandardSCU;
    }

    /**
     * @return Plain-text, human-readable, account code provided by the maintainer
     */
    public Option<String> getCode() {
        return code;
    }


    /**
     * @return Plain-text, human-readable, description provided by the maintainer
     */
    public Option<String> getDescription() {
        return description;
    }

    /**
     * @return if True/1, the account will not be displayed in the GnuCash GUI Accounts tab
     * and can be easily excluded from GnuCash GUI Reports.
     * if False/0, the account will be displayed in the GnuCash GUI Accounts tab.
     */
    public Option<Boolean> getHidden() {
        return hidden;
    }

    /**
     * @return if True/1, the account cannot be involved in transactions through splits
     * (ie it can only be the parent of other accounts).
     * if False/0, the account can have Splits referring to it (as well as be the parent of other accounts).
     * This field, if True, is also stored as a Slot under the key “placeholder” as a string “true”.
     */
    public Option<Boolean> getPlaceholder() {
        return placeholder;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(guid, account.guid) &&
                Objects.equals(name, account.name) &&
                Objects.equals(accountType, account.accountType) &&
                Objects.equals(commodity, account.commodity) &&
                Objects.equals(commoditySCU, account.commoditySCU) &&
                Objects.equals(parent, account.parent) &&
                Objects.equals(isNonStandardSCU, account.isNonStandardSCU) &&
                Objects.equals(code, account.code) &&
                Objects.equals(description, account.description) &&
                Objects.equals(hidden, account.hidden) &&
                Objects.equals(placeholder, account.placeholder);
    }

    @Override
    public int hashCode() {
        return Objects.hash(guid, name, accountType, commodity, commoditySCU, parent, isNonStandardSCU, code, description, hidden, placeholder);
    }

    @Override
    public String toString() {
        return "Account{" +
                "guid='" + guid + '\'' +
                ", name='" + name + '\'' +
                ", accountType='" + accountType + '\'' +
                ", commodity=" + commodity +
                ", commoditySCU='" + commoditySCU + '\'' +
                ", parent=" + parent +
                ", nonStdSCU='" + isNonStandardSCU + '\'' +
                ", code=" + code +
                ", description=" + description +
                ", hidden=" + hidden +
                ", placeholder=" + placeholder +
                '}';
    }
}
