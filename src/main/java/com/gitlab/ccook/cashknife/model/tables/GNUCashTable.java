package com.gitlab.ccook.cashknife.model.tables;

import com.gitlab.ccook.cashknife.model.elements.GNUCashElement;
import com.gitlab.ccook.cashknife.model.elements.Price;
import com.gitlab.ccook.cashknife.model.elements.Version;
import com.gitlab.ccook.cashknife.model.util.CashKnifeException;
import com.gitlab.ccook.cashknife.model.util.CloseableIterator;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GNUCashTable<T extends GNUCashElement> {
    private Class<T> cls;

    public GNUCashTable(Class<T> cls) {
        this.cls = cls;
    }

    public CloseableIterator getElementIterator( Connection conn) throws CashKnifeException {
        try {
            PreparedStatement preparedStatement = conn.prepareStatement("select * from " + this.getClass().getSimpleName().toLowerCase() + ";");
            ResultSet resultSet = preparedStatement.executeQuery();
            return new TIt(conn, resultSet);
        } catch (SQLException e) {
            throw new CashKnifeException(e);
        }
    }

    public class TIt extends CloseableIterator<T> {
        public TIt(Connection conn, ResultSet resultSet) {
            super(conn, resultSet);
        }

        @Override
        protected T nextElement(ResultSet s) throws CashKnifeException {
            try {
                return cls.getDeclaredConstructor(Connection.class, ResultSet.class).newInstance(conn, s);
            } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new CashKnifeException(e);
            }
        }
    }
}


