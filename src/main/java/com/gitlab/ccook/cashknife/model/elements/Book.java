/*
Copyright 2019 Cam Cook

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.gitlab.ccook.cashknife.model.elements;

import com.gitlab.ccook.cashknife.model.tables.Accounts;
import com.gitlab.ccook.cashknife.model.util.CashKnifeException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;


/**
 * Element: Book
 * Table: Books
 * Schema: CREATE TABLE books(guid text(32) PRIMARY KEY NOT NULL, root_account_guid text(32) NOT NULL, root_template_guid text(32) NOT NULL);
 */
public class Book extends GNUCashElement {
    /**
     * Labels used by SQL fields
     */
    enum Label {
        GUID,
        ROOT_ACCOUNT_GUID,
        ROOT_TEMPLATE_GUID;
    }

    /**
     * Book ID (GUID / Primary key)
     */
    private String guid;
    /**
     * Root Account associated with book
     */
    private Account rootAccount;
    private String rootTemplateGUID;


    public Book(String guid, Account rootAccount, String rootTemplateGUID) {
        this.guid = guid;
        this.rootAccount = rootAccount;
        this.rootTemplateGUID = rootTemplateGUID;
    }

    // used by Books.getElement/ Books.getElementIterator
    public Book(Connection conn, ResultSet s) throws SQLException, CashKnifeException {
        this(s.getString(Book.GUID),
                Accounts.getElement(conn, s.getString(Label.ROOT_ACCOUNT_GUID.name().toLowerCase())),
                s.getString(Label.ROOT_TEMPLATE_GUID.name().toLowerCase()));
    }

    /**
     * @return Book ID (GUID / Primary key)
     */
    public String getID() {
        return guid;
    }

    /**
     * @return Root Account associated with book
     */
    public Account getRootAccount() {
        return rootAccount;
    }

    public String getRootTemplateGUID() {
        return rootTemplateGUID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(guid, book.guid) &&
                Objects.equals(rootAccount, book.rootAccount) &&
                Objects.equals(rootTemplateGUID, book.rootTemplateGUID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(guid, rootAccount, rootTemplateGUID);
    }

    @Override
    public String toString() {
        return "Book{" +
                "guid='" + guid + '\'' +
                ", rootAccount=" + rootAccount +
                ", rootTemplateGUID='" + rootTemplateGUID + '\'' +
                '}';
    }
}
