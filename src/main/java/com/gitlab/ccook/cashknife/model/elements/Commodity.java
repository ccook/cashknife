package com.gitlab.ccook.cashknife.model.elements;


import com.gitlab.ccook.cashknife.model.util.CashKnifeException;
import com.gitlab.ccook.cashknife.model.util.Option;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

//CREATE TABLE commodities(guid text(32) PRIMARY KEY NOT NULL,
//                      namespace text(2048) NOT NULL,
//                      mnemonic text(2048) NOT NULL,
//                      fullname text(2048),
//                      cusip text(2048),
//                      fraction integer NOT NULL,
//                      quote_flag integer NOT NULL,
//                      quote_source text(2048),
//                      quote_tz text(2048));
public class Commodity extends GNUCashElement {
    public static String GUID = "guid";
    public static String NAMESPACE = "namespace";
    public static String MNEMONIC = "mnemonic";
    public static String FULL_NAME = "fullname";
    public static String CUSIP = "cusip";
    public static String FRACTION = "fraction";
    public static String QUOTE_FLAG = "quote_flag";
    public static String QUOTE_SOURCE = "quote_source";
    public static String QUOTE_TZ = "quote_tz";

    private String guid;
    private String namespace;
    private String mnemonic;
    private Option<String> fullName = new Option<>();
    private Option<String> cusip = new Option<>();
    private double fraction;
    private boolean quoteFlag;
    private Option<String> quoteSource = new Option<>();
    private Option<String> quoteTZ = new Option<>();

    public Commodity(String guid, String namespace, String mnemonic, String fullName, String cusip, int fraction, int quoteFlag, String quoteSource, String quoteTZ) {
        this.guid = guid;
        this.namespace = namespace;
        this.mnemonic = mnemonic;
        this.fullName = new Option<>(fullName);
        this.cusip = new Option<>(cusip);
        this.fraction = (1.0 / fraction);
        this.quoteFlag = quoteFlag == 1;
        this.quoteSource = new Option<>(quoteSource);
        this.quoteTZ = new Option<>(quoteTZ);
    }

    public Commodity(Connection conn, ResultSet s) throws SQLException {
        this(s.getString(Commodity.GUID),
                s.getString(Commodity.NAMESPACE),
                s.getString(Commodity.MNEMONIC),
                s.getString(Commodity.FULL_NAME),
                s.getString(Commodity.CUSIP),
                s.getInt(Commodity.FRACTION),
                s.getInt(Commodity.QUOTE_FLAG),
                s.getString(Commodity.QUOTE_SOURCE),
                s.getString(Commodity.QUOTE_TZ));
    }


    public String getGUID() {
        return guid;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getMnemonic() {
        return mnemonic;
    }

    public Option<String> getFullName() {
        return fullName;
    }

    public Option<String> getCusip() {
        return cusip;
    }

    public double getFraction() {
        return fraction;
    }

    public boolean doesGetQuotes() {
        return quoteFlag;
    }

    public Option<String> getQuoteSource() {
        return quoteSource;
    }

    public Option<String> getQuoteTZ() {
        return quoteTZ;
    }

    @Override
    public String toString() {
        return "Commodity{" +
                "guid='" + guid + '\'' +
                ", namespace='" + namespace + '\'' +
                ", mnemonic='" + mnemonic + '\'' +
                ", fullName=" + fullName +
                ", cusip=" + cusip +
                ", fraction=" + fraction +
                ", quoteFlag=" + quoteFlag +
                ", quoteSource=" + quoteSource +
                ", quoteTZ=" + quoteTZ +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Commodity commodity = (Commodity) o;
        return Double.compare(commodity.fraction, fraction) == 0 &&
                quoteFlag == commodity.quoteFlag &&
                Objects.equals(guid, commodity.guid) &&
                Objects.equals(namespace, commodity.namespace) &&
                Objects.equals(mnemonic, commodity.mnemonic) &&
                Objects.equals(fullName, commodity.fullName) &&
                Objects.equals(cusip, commodity.cusip) &&
                Objects.equals(quoteSource, commodity.quoteSource) &&
                Objects.equals(quoteTZ, commodity.quoteTZ);
    }

    @Override
    public int hashCode() {
        return Objects.hash(guid, namespace, mnemonic, fullName, cusip, fraction, quoteFlag, quoteSource, quoteTZ);
    }
}
