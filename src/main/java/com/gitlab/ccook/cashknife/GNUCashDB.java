package com.gitlab.ccook.cashknife;

import com.gitlab.ccook.cashknife.model.elements.*;
import com.gitlab.ccook.cashknife.model.tables.*;
import com.gitlab.ccook.cashknife.model.util.CashKnifeException;
import com.gitlab.ccook.cashknife.model.util.CloseableIterator;
import com.gitlab.ccook.cashknife.model.util.SQLUtils;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class GNUCashDB implements Closeable {

    Connection conn;

    public GNUCashDB(File f) throws CashKnifeException {
        conn = SQLUtils.connect(f);
    }

    public CloseableIterator<Version> getVersions() throws CashKnifeException {
        return new Versions().getElementIterator(conn);
    }

    public CloseableIterator<Book> getBooks() throws CashKnifeException {
        return new Books().getElementIterator(conn);
    }

    public CloseableIterator<Commodity> getCommodities() throws CashKnifeException {
        return new Commodities().getElementIterator(conn);
    }

    public CloseableIterator<Price> getPrices() throws CashKnifeException {
        return new Prices().getElementIterator(conn);
    }

    public CloseableIterator<Account> getAccounts() throws CashKnifeException {
        return new Accounts().getElementIterator(conn);
    }

    public CloseableIterator<Transaction> getTransactions() throws CashKnifeException {
        return new Transactions().getElementIterator(conn);
    }

    public CloseableIterator<Split> getSplits() throws CashKnifeException {
        return new Splits().getElementIterator(conn);
    }

    @Override
    public void close() throws IOException {
        try {
            conn.close();
        } catch (SQLException e) {
            throw new IOException(e);
        }
    }
}
