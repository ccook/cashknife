package com.gitlab.ccook.cashknife;


import com.gitlab.ccook.cashknife.model.elements.*;
import com.gitlab.ccook.cashknife.model.util.CloseableIterator;
import com.gitlab.ccook.cashknife.model.util.Option;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

public class Test {

    public static void main(String[] args) throws Exception {
        File f = new File("/home/cam/household/backup.gnucash");

        GNUCashDB gnuCashDB = new GNUCashDB(f);
/*        CloseableIterator<Split> s = gnuCashDB.getSplits();
        while (s.hasNext()) {
            Split next = s.next();
            Transaction transaction = next.getTransaction();
            Commodity currency = transaction.getCurrency();
            Option<String> description = transaction.getDescription();
            Option<String> enterDate = transaction.getEnterDate();
            BigInteger quantityDenom = next.getQuantityDenom();
            BigInteger quantityNum = next.getQuantityNum();
            BigInteger d = quantityNum.divide(quantityDenom);//a
            Account account = next.getAccount();
            String accName = account.getName();
            System.out.println(enterDate + "\t|\t" + accName + "\t|\t" + description + "\t|\t" + d + " " + currency.getFullName());


            *//*    System.out.println(s.next());*//*
        }
        s.close();*/
        CloseableIterator<Transaction> t = gnuCashDB.getTransactions();
        while (t.hasNext()) {
            // System.out.println(t.next());
        }
        t.close();
        CloseableIterator<Account> a = gnuCashDB.getAccounts();
        Set<String> types = new HashSet<>();
        while (a.hasNext()) {
            types.add(a.next().getAccountType());
        }
        for(String ty: types){
            System.out.println(ty);
        }
        a.close();
        CloseableIterator<Version> v = gnuCashDB.getVersions();
        while (v.hasNext()) {
            //  System.out.println(v.next());
        }
        v.close();
        CloseableIterator<Book> b = gnuCashDB.getBooks();
        while (b.hasNext()) {
            //   System.out.println(b.next());
        }
        b.close();
        CloseableIterator<Price> p = gnuCashDB.getPrices();
        while (p.hasNext()) {
            //  System.out.println(p.next());
        }
        p.close();
        ;
        ;
     /*   Connection connect = SQLUtils.connect(f);
        PreparedStatement preparedStatement = connect.gett*/
/*        ResultSet execute = preparedStatement.executeQuery();
        System.out.println(execute);*/

    }
}
